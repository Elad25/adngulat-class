import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import {UsersService} from './users/users.service';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PostsComponent } from './posts/posts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { AngularFireModule } from 'angularfire2'; // for firebase

export const firebaseConfig = {
    apiKey: "AIzaSyDQLfhQ_fcq_FWDGQOiVDLaZ574DRSUE70",
    authDomain: "angular-24a28.firebaseapp.com",
    databaseURL: "https://angular-24a28.firebaseio.com",
    storageBucket: "angular-24a28.appspot.com",
    messagingSenderId: "66117733144"
}


const appRoutes:Routes= [
  {path: 'users', component:UsersComponent},
  {path: 'posts', component:PostsComponent},
  {path: '', component:UsersComponent},
  {path: '**', component:PageNotFoundComponent},
  ]

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    UserFormComponent,
    PageNotFoundComponent   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig) // for firebase
  ],
  providers: [UsersService], //לא לשכוח להוסיף את זה בשביל שיעבוד
  bootstrap: [AppComponent]
})
export class AppModule { }
