import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.service';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class UsersComponent implements OnInit {

isLoading:Boolean= true;
users;

currentUser;

select(user){
this.currentUser = user;  // li מגיע מ user 
}

  constructor(private _usersService:UsersService) { }

addUser(user){
    //this.users.push(user) // only local not from firebase
    this._usersService.addUser(user);
  }

deleteUser(user){
   //this.users.splice(    // we delete this because we want delete from firebase 
   //  this.users.indexOf(user),1 // indexOf(user) is the location and 1 is the quantity
  //  )

  this._usersService.deleteUser(user);
  }

updateUser(user){
  this._usersService.updateUser(user);

}


  ngOnInit()  // פונקציה שמופעלת בזמן טעינת הקובץ, נרצה שהנתונים יטענו
  {
    this._usersService.getUsersFromApi().subscribe(usersData => {this.users = usersData.result; this.isLoading = false; console.log(this.users)}); //סוגריים מסוסלים כי יש כמה פעולות ולא צריך נקודה פסיק בסוף הפעולה האחרונה
  
  }

}
